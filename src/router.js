import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import About from "./views/About.vue";
import Work from "./views/Work.vue";
import Contact from "./views/Contact.vue";
import Impressum from "./views/Impressum.vue";
import Datenschutz from "./views/Datenschutz.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        title: 'Valentina Huelsmann',
        metaTags: [
          {
            name: 'Home Page',
            content: 'The home page'
          },
        ]
      },
    },
    {
      path: "/home",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/work",
      name: "work",
      component: Work
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    },
    {
      path: "/impressum",
      name: "impressum",
      component: Impressum
    },
    {
      path: "/Datenschutz",
      name: "Datenschutz",
      component: Datenschutz
    }
  ]
});

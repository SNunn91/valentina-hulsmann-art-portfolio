import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme:{
    themes: {
      light: {
        primary: '#8AC0DE',
        secondary: '#f5f5f5',
        tertiary: '#f1f1f1',
      },
    },
  },
  icons: {
    iconfont: 'mdi',
  },
});

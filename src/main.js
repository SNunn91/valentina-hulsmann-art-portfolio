import "@/assets/scss/site.scss";

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./registerServiceWorker";
import vuetify from './plugins/vuetify';

import AOS from 'aos'
import 'aos/dist/aos.css'


// import VueRellax from 'vue-rellax'

import VueSilentbox from 'vue-silentbox'




Vue.use(VueSilentbox);

 
// Vue.use(VueRellax)

Vue.config.productionTip = false;

new Vue({
  created () {
    AOS.init()
  },
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
